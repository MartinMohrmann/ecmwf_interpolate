#!/usr/bin/env python

###############################################################################
# Data correlation script by Martin Mohrmann                                  #
# Requirements: ecmfapi, netCDF4, numpy                                       #
#                                                                             #
# Sample code to run:                                                         #
# import download_ECMWFmet_netcdf as Interim                                  #
# import datetime                                                             #
# starttime = datetime.datetime(2017,1,1,19,30)                               #
# mydata = Interim.EraInterim(starttime=starttime)                            #
# Now the data can be interpolated to get best approximation for coordinates  #
# result = mydata.Interpolate(295.1, -60.1, mydate)  .                        #
###############################################################################

import datetime
# saving results in 
filename = 'ECMWFmet3h.nc'

class EraInterim(object):

    def __init__(self, starttime, interval=0, longitude='all', latitude='all'):
        """ initiate the class with the given longitude, latitude
        and the time you want to fetch the data for. 
        dtime might be an interval 

        Input arguments:
        longitude: 2-tuple in range from 0 to 360, eg. (270, 280)
        latitude : 2-tuple in range from  -90 to  90, eg. (-50, -75)
        starttime: eg python datetime.datetime(1970, 1, 1, 19, 30) 
        interval: eg. datetime.timedelta(days=365)"""
        if interval == 0:
            interval = datetime.timedelta(days=0)

        self.longitude = longitude
        self.latitude  = latitude
        self.starttime = starttime
        self.interval = datetime.timedelta(days=interval)
        self.endtime   = starttime + self.interval

    def Retrieve(self):
        """ retrieve the data from the ERA-INTERIM model
        for a general purpose method, it could take the 
        input parameters latitude, longitude, datetime
        data is retrieved on a 0.75° grid """
        from ecmwfapi import ECMWFDataServer
        server = ECMWFDataServer()
        if self.longitude == 'all':
            lon1 =   0
            lon2 = 360
        else:
            lon1 = self.longitude[0]
            lon2 = self.longitude[1]
        if self.latitude == 'all':
            lat1 =  90
            lat2 = -90
        else:
            lat1 = self.latitude[0]
            lat2 = self.latitude[1]
        if self.starttime == self.endtime:
            # the user has not specified an intervall
            date = self.starttime.strftime('%Y-%m-%d')
        else:
            # the user has specified an interval
            date = self.starttime.strftime('%Y-%m-%d')+'/to/'+self.endtime.strftime('%Y-%m-%d')
        server.retrieve({
            "class": "ei",
            "dataset": "interim",
            "stream": "oper",
            "expver": "1",
            "date": date, #"2017-08-01/to/2017-12-31",
            "type": "fc",
            "levtype": "sfc",
            "param": ("228.128/57.128/177.128/147.128/146.128/229.128/230.128/"+
                "167.128/231.128/34.128/31.128/189.128/165.128/166.128"),#/121.200"),#/231.128"),
                #/228.178"),#/"+204.200
            "step": "3/6/9/12",                   # accumulation over 12 hours
            "time": "00:00:00/12:00:00",          # from the two forecasts initialized at 00:00 and 12:00
            "grid": "0.75/0.75",
            "area": '{0}/{1}/{2}/{3}'.format(lat1, lon1, lat2, lon2),             # (in N/W/S/E)
            "format": "netcdf",
            "target": filename,            # change this to your output file name
        })

    def Interpolate(self, longitude, latitude, datetime='all'):
        """ interpolates the downloaded original data with simple
        2-dimensional linear interpolation, to get the closest 
        possible approximation for the given coordinates """
        import netCDF4
        dataset = netCDF4.Dataset(filename)
        # find the coordinate grid points sourrounding the coordinate of interest
        resultdict = {}
        gridlatitudes  = dataset.variables.get('latitude' )[:].data
        gridlongitudes = dataset.variables.get('longitude')[:].data
        latindex1, latindex2   = findNeighbours(
            latitude ,dataset.variables.get('latitude')[:].data)
        lonindex1, lonindex2   = findNeighbours(
            longitude,dataset.variables.get('longitude')[:].data)
        # starting the bilinear interpolation process
        for key in dataset.variables.keys():
            if key in ['latitude', 'longitude', 'time']:
                continue
            try:
                fxy1 = (((gridlongitudes[lonindex2]-longitude)/
                    (gridlongitudes[lonindex2]-gridlongitudes[lonindex1]))*
                    dataset.variables.get(key)[:,lonindex1,latindex1] + 
                    ((longitude-gridlongitudes[lonindex1])/(gridlongitudes[lonindex2]-
                    gridlongitudes[lonindex1]))*dataset.variables.get(key)[:,lonindex2,latindex1])
                fxy2 = (((gridlongitudes[lonindex2]-longitude)/
                    (gridlongitudes[lonindex2]-gridlongitudes[lonindex1]))*
                    dataset.variables.get(key)[:,lonindex1,latindex2] + 
                    ((longitude-gridlongitudes[lonindex1])/(gridlongitudes[lonindex2]-
                    gridlongitudes[lonindex1]))*dataset.variables.get(key)[:,lonindex2,latindex2])
                # print('Tuple:',fxy1, fxy2)
                fxy = (gridlatitudes[latindex2]-latitude)/(gridlatitudes[latindex2]-gridlatitudes[latindex1])*fxy1 + \
                      (latitude-gridlatitudes[latindex1])/(gridlatitudes[latindex2]-gridlatitudes[latindex1])*fxy2
                if key == 't2m':
                    print('just processed t2m')
                    #import pdb; pdb.set_trace()
                # if key == 'uvb':
                #     import pdb; pdb.set_trace()
                resultdict[key]=fxy
            except:
                raise('failed interpolating {0}. Is it a 2D gridded variable? It has to be.')
            finally:
                print('finished bilinear interpolation of {0}'.format(key))
                resultdict['time'] = dataset.variables.get('time')[:]
        return resultdict

    def Convert(self, resultdict):
        """ Some of the downloaded values are accumulated values
        for most modelling cases, we would rather want instantaneous
        values, for example Watt instead of Joule. The Convert() 
        method converts cummulated values into instantenous ones. """
        #import netCDF4
        #dataset = netCDF4.Dataset('ECMWFmet3h.nc')
        ursprung = datetime.datetime(1900,1,1)
        resultdict['pythondatetime'] = []
        resultdict['hours'] = []
        for index, mytime in enumerate(resultdict['time'].data):
            resultdict['pythondatetime'].append(ursprung + datetime.timedelta(hours=int(mytime)))
            resultdict['hours'].append(resultdict['pythondatetime'][index].hour)
        # import pdb; pdb.set_trace()


        for key in ['uvb', 'str', 'slhf', 'sshf', 'tp']:
            # this convert function is currently written for the 3hourly output.
            # By dividing by dt, a 3hour 'instantenous' value is created

            # See the article ERA-Interim: 'time' and 'steps', and instantaneous, 
            # accumulated and min/max parameters. Note example 5 to gain the highest
            # possible resolution
            for index, value in enumerate(resultdict[key]):
                if resultdict['hours'][index] in [3,15]:
                    resultdict[key][index] = value/(3*60*60)
                if resultdict['hours'][index] in [6,18]:
                    resultdict[key][index] = (value-resultdict[key][index-1])/(3*60*60)
                if resultdict['hours'][index] in [9,21]:
                    resultdict[key][index] = (value-resultdict[key][index-1])/(3*60*60)
                if resultdict['hours'][index] in [12,0]:
                    resultdict[key][index] = (value-resultdict[key][index-1])/(3*60*60)


            # This function does not change the labels and descriptions in the .nc file
        print('finished converting to instantaneous variables')
        return resultdict

    def MakeMet(self, resultdict):
        """ Extracts the relevant values for the PWP model and saves them in a Matlab ©
        compatible format for the Matlab version of the PWP model"""
        import scipy.io
        import netCDF4
        print(netCDF4.Dataset(filename).variables.get('t2m')[300:310,4,4])
        print(resultdict['t2m'][300:310])
        # map some variables to different keyword for matlab input
        # and get the time format right. PWP wants yeardays with decimal
        # representation 
        ursprung = datetime.datetime(1900,1,1)
        # Time two is just a helper variable for conversion of time. 
        # It has no meaning and no future use.
        resultdict['time2'] = []
        # See the article ERA-Interim: 'time' and 'steps', and instantaneous, 
        # accumulated and min/max parameters
        for index, mytime in enumerate(resultdict['time'].data):
            resultdict['time2'].append(ursprung + datetime.timedelta(hours=int(mytime)))
            resultdict['time2'][index] = (
                resultdict['time2'][index].timetuple().tm_yday + 
                resultdict['time2'][index].hour/24)
        # import pdb; pdb.set_trace()

        resultdict['time']   = resultdict['time2']
        resultdict['precip'] = resultdict['tp']
        resultdict['qlat']   = resultdict['slhf']
        resultdict['qsens']  = resultdict['ishf']
        resultdict['tx']     = resultdict['iews']
        resultdict['ty']     = resultdict['inss']
        resultdict['lw']     = resultdict['str']
        resultdict['sw']     = resultdict['uvb']
        
        # remove date values, because mat files can not save them.
        del(resultdict['pythondatetime'])
        # Since the matlab script wants it all wrapped up into 'met' variable...
        resultdict = {'met':resultdict}
        scipy.io.savemat('metinput.mat', resultdict)
        print('finished writing PWP input file')


def findNeighbours(value, array):
    """ this function locates a value (for example a measured latitude)
    in an array of values (for example a gridded output modell). 
    The return values are the grid neighbours of the measurement 
    value, that can be used for linear interpolation or nearest
    neighbour treatment """
    # import numpy

    # finding the nearest neighbour first
    import numpy
    index1 = (numpy.abs(array-value)).argmin()
    array[index1] = numpy.inf
    index2 = (numpy.abs(array-value)).argmin()
    print('index12', index1, index2)
    if (index1 or index2) == 0 or (index1 or index2) == (len(array) - 1):
        raise("the closest gridvalue is right at the boundary of the array."
            "This propably means that you specified the area too small in the Retrieve"
            "Method. Return at own risk.")
    return index1, index2

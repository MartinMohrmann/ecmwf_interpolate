import download_ECMWFmet_netcdf as Interim
import datetime 
import netCDF4

starttime = datetime.datetime(2016,12,28,0,0)
latitude = (-62,-67)
longitude = (-1,3)
# Attention, crossing a year does not work here because 
# of the yearday time format in pwp. The new year days would
# be counted as earlier index if no adjustments where made
interval = 368
mydataset = Interim.EraInterim(starttime, interval, longitude, latitude)
# mydataset.Retrieve()
resultdict = mydataset.Interpolate(longitude=0.4260, latitude=-64.9380)
resultdict = mydataset.Convert(resultdict)
mydataset.MakeMet(resultdict)

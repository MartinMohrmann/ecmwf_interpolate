# ECMWF_interpolate

The program downloads important atmospheric parameters and ocean-air-fluxes from the ECMWF-Interim Model. Additionally, the values can be interpolated to get the best estimation for a specific area. 
It makes spatial data-correlation of measurement results with the Interim model data easier.  

# Get Started

Assume you are intersted in the model parameters for the coordinates x,y at the time t:
For this example, we make up some example numerical values:
longitude = 295.1, latitude = -60.1, time = 01.01.2015

The first thing to do is downloading the data from the ECMWFDataServer. It is done by specifying the corner coordinates of the area you are interested in. Make the area so big, that all your points of interest are in the area, but also as small as possible to minimize the download-time. By Downloading only once and specifying the area as good as possible, you save the resources of the ECMWF people.

import the library from this repository
```python
import download_ECMWFmet_netcdf as Interim
```
you will need some other python libraries also. Then procceed with specifying a date, an interval (optional), and corner coordinates
```python                          
import datetime                                                             
starttime = datetime.datetime(2017,1,1,19,30)                               
mydata = Interim.EraInterim(starttime=starttime, intervall=datetime.timedelta(days=30), longitude=(290., 300.), latitude=(-55.,-65.))
```
Now the data can be interpolated to get best approximation for coordinates
```python
result = mydata.Interpolate(295.1, -60.1)
```

# Clarification of the downloaded parameters

| Property  | Mod. Code| units      | Description                                                                |
|-----------|----------|------------|----------------------------------------------------------------------------|
|precip     | 228.128  |[m]         |Precipation                                                                 |
|uvb        |      57  |[J/m**2]    |downward UV                                                                 |
|str        |     177  |[J/m**2]    |surface net thermal radiation                                               |
|slhf       |     147  |[J/m**2]    |surface upward latent heat flux                                             |
|sshf       |     146  |[J/m**2]    |surface upward sensible heat flux                                           |
|iews       |     229  |[N/m**2]    |instant eastward turbulent surface stress                                   |
|inss       |     230  |[N/m**2]    |instant northward turbulent surface stress                                  |
|t2m        | 167.128  |            |temerature at 2m height above sea surface                                   |
|ishf       | 231.128  |[W/m**2]    |istantenous surface sensible heat flux                                      |
|sst        |  34.128  |[K]         |sea surface temperature                                                     |
|ci         |  31.128  |[0-1]       |sea ice coverage                                                            |
|sund       | 189.128  |[s]         |sunshine duration                                                           |

